package com.example.dhillankr.tabrecycler.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FavoritModel extends RealmObject {
    @PrimaryKey
    private Integer id;
    private String judul;
    private String gambar;
    private String desc;
    private String area;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
