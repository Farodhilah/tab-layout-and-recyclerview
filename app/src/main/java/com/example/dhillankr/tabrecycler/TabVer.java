package com.example.dhillankr.tabrecycler;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dhillankr.tabrecycler.Adapter.FavoritAdapter;
import com.example.dhillankr.tabrecycler.Models.FavoritModel;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class TabVer extends Fragment {

    FavoritAdapter vAdapter;
    RecyclerView recyclerView2;

    Realm realm;
    RealmHelper realmHelper;
    List<FavoritModel> favoritModel;
    SwipeRefreshLayout sw;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view2 = inflater.inflate(R.layout.tabver, container, false);

        recyclerView2 = view2.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView2.setLayoutManager(layoutManager);
        sw = view2.findViewById(R.id.swiper);

        //Set up Realm
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        favoritModel = realmHelper.getAllFavorit();
        vAdapter = new FavoritAdapter(getActivity(), favoritModel);
        recyclerView2.setAdapter(vAdapter);
//        sw.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
//            public void onRefresh(){
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        sw.setRefreshing(false);
//
//                        favoritModel = realmHelper.getAllFavorit();
//                        vAdapter = new FavoritAdapter(getActivity(), favoritModel);
//                        recyclerView2.setAdapter(vAdapter);
//
//                    }
//                },2000);
//            }
//        });
        return view2;

    }

    @Override  
    public void onResume() {
        super.onResume();
        sw.setRefreshing(false);

        favoritModel = realmHelper.getAllFavorit();
        vAdapter = new FavoritAdapter(getActivity(), favoritModel);
        recyclerView2.setAdapter(vAdapter);

    }

}
