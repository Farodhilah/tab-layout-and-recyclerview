package com.example.dhillankr.tabrecycler.rest;

import com.example.dhillankr.tabrecycler.Models.LatestItem;

import retrofit2.Call;
import retrofit2.http.GET;

public interface LatestInterface {
    @GET("latest.php")
    Call<LatestItem> getData();
}
