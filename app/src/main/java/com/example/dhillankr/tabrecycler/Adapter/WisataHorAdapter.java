package com.example.dhillankr.tabrecycler.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dhillankr.tabrecycler.Models.CategoriItem;
import com.example.dhillankr.tabrecycler.Models.Warung;
import com.example.dhillankr.tabrecycler.R;

import java.util.List;

public class WisataHorAdapter extends RecyclerView.Adapter<WisataHorAdapter.ViewHolder> {
    CategoriItem[] categoriItems;
    Context context;
    private List<Warung> warungs;

    public WisataHorAdapter(CategoriItem[] categoriItems, Context context, List<Warung> warungs) {
        this.categoriItems = categoriItems;
        this.context = context;
        this.warungs = warungs;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listhorizontal, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvjudul.setText(categoriItems[position].getStrCategory());
//        Picasso.get().load(categoriItems[position].getStrCategoryThumb()).into(holder.ivFoto);
        final Warung warung = warungs.get(position);
        holder.ivFoto.setImageURI(Uri.parse(warung.getImg()));
        holder.tvjudul.setText(warungs.get(position).getNama());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vw) {

                OpenDetail(warungs.get(position).getNama(), warungs.get(position).getImg());
            }
        });



    }

    private void OpenDetail(String foto, String nama) {
        Intent detail1 = new Intent(context, Detail2Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString("Title", foto);
        bundle.putString("Category", nama);
        bundle.putInt("Id", 1);
        detail1.putExtras(bundle);
        context.startActivity(detail1);
    }


    @Override
    public int getItemCount() {
        if (categoriItems != null)
            return warungs.size();
        return 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivFoto;
        TextView tvjudul;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ivFoto = itemView.findViewById(R.id.imageView);
            tvjudul = itemView.findViewById(R.id.tjudul);
            imageView = itemView.findViewById(R.id.imageView);

        }
    }
}
