package com.example.dhillankr.tabrecycler.Adapter;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dhillankr.tabrecycler.R;
import com.example.dhillankr.tabrecycler.RealmHelper;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Detail2Activity extends AppCompatActivity {
    Realm realm;
    RealmHelper realmHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail2);

        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        final Bundle bundle = getIntent().getExtras();

        ImageView img1 = findViewById(R.id.image3);
        TextView tv1 = findViewById(R.id.tv1);

        tv1.setText(bundle.getString("Title"));
        img1.setImageURI(Uri.parse(bundle.getString("Category")));
    }
}
