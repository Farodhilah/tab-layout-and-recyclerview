package com.example.dhillankr.tabrecycler.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dhillankr.tabrecycler.DetailActivity;
import com.example.dhillankr.tabrecycler.Models.FavoritModel;
import com.example.dhillankr.tabrecycler.Models.MealsItem;
import com.example.dhillankr.tabrecycler.R;
import com.example.dhillankr.tabrecycler.RealmHelper;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class WisataVerAdapter extends RecyclerView.Adapter<WisataVerAdapter.ViewHolder> {
    MealsItem[] mealsItems;
    Context konteks;
    Realm realm;
    RealmHelper realmHelper;
    FavoritModel favoritModel;

    public WisataVerAdapter(MealsItem[] mealsItems, Context konteks) {
        this.mealsItems = mealsItems;
        this.konteks = konteks;
    }

    @Override
    public WisataVerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View t = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listvertical, parent, false);
        WisataVerAdapter.ViewHolder ts = new WisataVerAdapter.ViewHolder(t);
        return ts;
    }

    @Override
    public void onBindViewHolder(WisataVerAdapter.ViewHolder holder, final int position) {
        holder.meal.setText(mealsItems[position].getStrMeal());
        holder.categori.setText(mealsItems[position].getStrCategory());
        holder.area.setText(mealsItems[position].getStrArea());
        Picasso.get().load(mealsItems[position].getStrMealThumb()).into(holder.gambar);

        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vw) {
//                favoritModel = new FavoritModel();
//                favoritModel.setJudul(mealsItems[position].getStrMeal());
//                favoritModel.setDesc(mealsItems[position].getStrCategory());
//                favoritModel.setGambar(mealsItems[position].getStrMealThumb());
//                favoritModel.setArea(mealsItems[position].getStrArea());
//
//                realmHelper = new RealmHelper(realm);
//                realmHelper.save(favoritModel);
//
//                Toast.makeText(konteks, "data tambah", Toast.LENGTH_SHORT).show();
                OpenDetail(mealsItems[position].getStrCategory(), mealsItems[position].getStrArea(), mealsItems[position].getStrMealThumb());
            }
        });
    }

    private void OpenDetail(String title, String category, String MT) {
        Intent detail1 = new Intent(konteks, DetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("Title", title);
        bundle.putString("Category", category);
        bundle.putString("MT", MT);
        bundle.putInt("Id", 1);
        detail1.putExtras(bundle);
        konteks.startActivity(detail1);


    }

    @Override
    public int getItemCount() {
        if (mealsItems != null)
            return mealsItems.length;
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView gambar;
        TextView meal;
        TextView categori;
        TextView area;
        CardView cardview;

        public ViewHolder(View itemView) {
            super(itemView);
            gambar = itemView.findViewById(R.id.igambar);
            meal = itemView.findViewById(R.id.meal);
            cardview = itemView.findViewById(R.id.cardview);
            categori = itemView.findViewById(R.id.categori);
            area = itemView.findViewById(R.id.area);
        }
    }
}
