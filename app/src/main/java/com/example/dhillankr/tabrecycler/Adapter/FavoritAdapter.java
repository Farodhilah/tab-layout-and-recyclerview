package com.example.dhillankr.tabrecycler.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dhillankr.tabrecycler.Models.FavoritModel;
import com.example.dhillankr.tabrecycler.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavoritAdapter extends RecyclerView.Adapter<FavoritAdapter.ViewHolder> {

    private Context context;
    private List<FavoritModel> favoritModels;

    public FavoritAdapter(Context context, List<FavoritModel> favoriteModels) {
        this.context = context;
        this.favoritModels = favoriteModels;
    }

    @NonNull
    @Override
    public FavoritAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.listvertical, viewGroup, false);
        return new FavoritAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final FavoritModel mealsItems = favoritModels.get(position);
        viewHolder.judul.setText(mealsItems.getJudul());
        viewHolder.categori.setText(mealsItems.getDesc());
        viewHolder.area.setText(mealsItems.getArea());
        Picasso.get().load(mealsItems.getGambar()).into(viewHolder.gambar);

        viewHolder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vw) {
                Toast.makeText(context, "" + mealsItems.getJudul(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return favoritModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView gambar;
        TextView judul;
        TextView categori;
        TextView area;
        CardView cardview;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            gambar = itemView.findViewById(R.id.igambar);
            judul = itemView.findViewById(R.id.meal);
            cardview = itemView.findViewById(R.id.cardview);
            categori = itemView.findViewById(R.id.categori);
            area = itemView.findViewById(R.id.area);
        }
    }
}
