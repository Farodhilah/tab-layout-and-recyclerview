package com.example.dhillankr.tabrecycler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dhillankr.tabrecycler.Models.FavoritModel;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DetailActivity extends AppCompatActivity {
    Realm realm;
    RealmHelper realmHelper;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        setSupportActionBar(toolbar);

        //Setup Realm
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        final Bundle bundle = getIntent().getExtras();

        ImageView img1 = findViewById(R.id.image1);
        TextView tv1 = findViewById(R.id.tvJudul);
        TextView tv2 = findViewById(R.id.tvCat);
        final ImageView img2 = findViewById(R.id.image2);

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img2.setColorFilter(getApplication().getResources().getColor(R.color.colorAccent));
                FavoritModel favoriteModel = new FavoritModel();
                favoriteModel.setGambar(bundle.getString("MT"));
                favoriteModel.setJudul(bundle.getString("Title"));
                favoriteModel.setDesc(bundle.getString("Category"));
                realmHelper.save(favoriteModel);
            }
        });

        tv1.setText(bundle.getString("Title"));
        tv2.setText(bundle.getString("Category"));
        Picasso.get().load(bundle.getString("MT")).into(img1);

//        Toast.makeText(getApplicationContext(),bundle.getString("Title"),Toast.LENGTH_SHORT).show();
//        Toast.makeText(getApplicationContext(),bundle.getString("Category"),Toast.LENGTH_SHORT).show();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
