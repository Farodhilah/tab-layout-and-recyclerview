package com.example.dhillankr.tabrecycler.Models;

import com.google.gson.annotations.SerializedName;

public class LatestItem {
    @SerializedName("meals")
    MealsItem[] mealsItems;

    public MealsItem[] getMealsItems() {
        return mealsItems;
    }

}
