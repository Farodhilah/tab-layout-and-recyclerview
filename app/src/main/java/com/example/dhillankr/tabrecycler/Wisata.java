package com.example.dhillankr.tabrecycler;

import android.graphics.drawable.Drawable;

public class Wisata {
    public String judul;
    public String desc;
    public Drawable gambar;

    public Wisata(String judul, String desc, Drawable gambar) {
        this.judul = judul;
        this.desc = desc;
        this.gambar = gambar;
    }


}
