package com.example.dhillankr.tabrecycler;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dhillankr.tabrecycler.Adapter.WisataHorAdapter;
import com.example.dhillankr.tabrecycler.Adapter.WisataVerAdapter;
import com.example.dhillankr.tabrecycler.Models.CategoriItem;
import com.example.dhillankr.tabrecycler.Models.CategoriesItem;
import com.example.dhillankr.tabrecycler.Models.LatestItem;
import com.example.dhillankr.tabrecycler.Models.MealsItem;
import com.example.dhillankr.tabrecycler.Models.Warung;
import com.example.dhillankr.tabrecycler.rest.ApiClient;
import com.example.dhillankr.tabrecycler.rest.CategoriesInterface;
import com.example.dhillankr.tabrecycler.rest.LatestInterface;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TabHorizon extends Fragment {


    ArrayList<Wisata> mList = new ArrayList<>();
    WisataHorAdapter mAdapter;
    WisataVerAdapter vAdapter;
    RecyclerView recyclerView2, recyclerView3;

    RealmHelper realmHelper;
    Realm realm;
    CategoriesInterface categoriesInterface;
    Call<CategoriesItem> categoriesItemCall;
    CategoriItem[] categoriItems;

    LatestInterface latestInterface;
    Call<LatestItem> latestItemCall;
    MealsItem[] mealsItem;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view1 = inflater.inflate(R.layout.tabhorizon, container, false);

        recyclerView2 = view1.findViewById(R.id.recyclerView2);
        recyclerView3 = view1.findViewById(R.id.recyclerView3);

        fillData();

        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        return view1;
    }


    private void fillData() {
        categoriesInterface = ApiClient.getClient().create(CategoriesInterface.class);
        categoriesItemCall = categoriesInterface.getData();

        latestInterface = ApiClient.getClient().create(LatestInterface.class);
        latestItemCall = latestInterface.getData();

        categoriesItemCall.enqueue(new Callback<CategoriesItem>() {
            @Override

            public void onResponse(Call<CategoriesItem> call, Response<CategoriesItem> response) {
                categoriItems = response.body().getCategoriItems();
                List<Warung> warungs = realmHelper.getAllWarung();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                recyclerView2.setLayoutManager(layoutManager);
                mAdapter = new WisataHorAdapter(categoriItems, getContext(), warungs);
                recyclerView2.setAdapter(mAdapter);

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<CategoriesItem> call, Throwable t) {
                Log.wtf("hasil", t.getMessage());
            }
        });


        latestInterface = ApiClient.getClient().create(LatestInterface.class);
        latestItemCall = latestInterface.getData();

        latestItemCall.enqueue(new Callback<LatestItem>() {
            @Override
            public void onResponse(Call<LatestItem> call, Response<LatestItem> response) {
                mealsItem = response.body().getMealsItems();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView3.setLayoutManager(layoutManager);
                vAdapter = new WisataVerAdapter(mealsItem, getContext());
                recyclerView3.setAdapter(vAdapter);

                mAdapter.notifyDataSetChanged();

            }


            @Override
            public void onFailure(Call<LatestItem> call, Throwable t) {
                Log.wtf("hasil", t.getMessage());
            }
        });

    }
}
