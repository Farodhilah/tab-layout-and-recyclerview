package com.example.dhillankr.tabrecycler.Models;

import com.google.gson.annotations.SerializedName;

public class CategoriesItem {
    @SerializedName("categories")
    CategoriItem[] categoriItems;

    public CategoriItem[] getCategoriItems() {
        return categoriItems;
    }

}
