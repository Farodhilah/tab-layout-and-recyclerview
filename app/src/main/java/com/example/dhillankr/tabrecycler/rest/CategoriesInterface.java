package com.example.dhillankr.tabrecycler.rest;

import com.example.dhillankr.tabrecycler.Models.CategoriesItem;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoriesInterface {
    @GET("categories.php")
    Call<CategoriesItem> getData();
}
